# TermQCM

Projet de RT0702 

## Instructions



### Prérequis

L'intégralité de l'application est conçue pour être executée via Docker, celui-ci est donc nécéssaire.
De même il est nécéssaire de disposer de Dockercompose

### Installation


```bash
git clone https://gitlab.com/Shadowx/termqcm.git
cd termqcm/
docker-compose up -d
```

## Execution

Une fois le build et les containers lancés, il suffit de se connecter sur termqcm_client_1 : 

```bash 
docker attach termqcm_client_1
```

Une fois dans le container,  la saisie de la touche "entrer" est attendu afin de lancer l'execution.

Deux comptes sont disponibles : 

* utest : ptest (affecté à la formation ASR)
* test2 : pwd2 (affecté à la formation DAS)

Ce second utilisateur n'accède à aucun QCM, la formation DAS n'ayant pas de QCM associée.

Plusieurs modules sont créés par défaut, le module RT0704 possède deux questionnaires dont :

* QCM1 un vrai questionnaire
* QCM2 ne contenant pas de question servant aux tests de la gestion des erreurs

Lors du lancement du QCM, il est a noter que la multi-selection de réponse est possible. De fait, les touches suivantes servent à :
* SPACE : selectionner une réponse
* ENTRER : valider la/les réponse(s)

A la fin du questionnaire, la note est automatiquement calculée.