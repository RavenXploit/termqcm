FROM python:3.7.2-alpine
COPY requirements.txt requirements.txt
WORKDIR .


RUN pip install -r requirements.txt

EXPOSE 5000