# coding: utf-8

import json
import os
import inquirer
import requests
import logging

from termcolor import colored

logger = logging.getLogger()

# Définiiton de la bannière affichée lors du lancement du client
banner = '\
\n\
\n\
\n\
\n\
 _______                   ____   _____ __  __      \n\
|__   __|                 / __ \ / ____|  \/  |     \n\
   | | ___ _ __ _ __ ___ | |  | | |    | \  / |     \n\
   | |/ _ \ \'__| \'_ ` _ \| |  | | |    | |\/| |   \n\
   | |  __/ |  | | | | | | |__| | |____| |  | |     \n\
   |_|\___|_|  |_| |_| |_|\___\_\ _____|_|  |_|    \n\
                                                    \n\
                                                    \n\
Created by : Romain ORTEGA & Kevin WEZEMAEL\n\
Vesion : 1.O \
\n\n \
'


#######################################################################################################################
# Déclaration des fonctions
#######################################################################################################################

# Définition des serveurs cibles
if 'DOCKER_AUTH_SERVER' in os.environ:
    AUTH_SERVER = 'auth'
    AUTH_PORT = '5000'
else:
    AUTH_SERVER = 'localhost'
    AUTH_PORT = '8081'


if 'DOCKER_CORRECTOR_SERVER' in os.environ:
    CORRECTOR_SERVER = 'corrector'
    CORRECTOR_PORT = '5000'
else:
    CORRECTOR_SERVER = 'localhost'
    CORRECTOR_PORT = '5000'


if 'DOCKER_QCM_SERVER' in os.environ:
    SERVER = 'server'
    PORT = '5000'
else:
    SERVER = 'localhost'
    PORT = '8080'


def login():
    questions = [
        inquirer.Text('login', message="Quel est votre identifiant"),
        inquirer.Text('password', message="Quel est votre mot de passe"),
    ]
    credentials = inquirer.prompt(questions)

    if not credentials['password'] or not credentials['password']:
        logger.error(colored('Veuillez saisir vos informations de connexion', color='red'))
        return False

    target = 'http://' + AUTH_SERVER + ':' + AUTH_PORT + '/login'
    r = requests.post(
        target,
        data=json.dumps(credentials))

    if r.status_code == 200:
        return json.loads(r.content)
    else:
        logger.error(colored('Informations de connexion incorrectes.', color='red'))
        return False




def get_modules(user):
    r = requests.post('http://'+SERVER+':' + PORT + '/modules', data=json.dumps(user))

    if r.status_code == 200:
        return json.loads(r.content)

    else:
        logger.fatal(colored('Anomalie lors de la récupération des modules.', color='red'))



def afficher_modules(modules):

    intitules=[]
    for module in modules:
        intitules.append(module['intitule'])

    #  On rajoute la possibilité de revenir en arrière à la liste des choix
    intitules.append("Quitter")

    # On construit notre question inquirer
    choix = [
        inquirer.List('res',
                      message="Quel est le module désiré ?",
                      choices=intitules,
                      ),
    ]

    # On demande à l'utilisateur de faire son choix via inquirer
    choix = inquirer.prompt(choix)

    return choix


def get_qcms(modules, intitule):

    for module in modules:
        if module['intitule'] == intitule:
            r = requests.get('http://' + SERVER + ':' + PORT + '/qcms/' + module['code'])
            return json.loads(r.content)


def afficher_qcms(qcms):

    intitules = []
    for qcm in qcms:
        intitules.append(qcm['name'])

    #  On rajoute la possibilité de revenir en arrière à la liste des choix
    intitules.append("Retour")

    # On construit notre question inquirer
    choix = [
        inquirer.List('res',
                      message="Quel est le QCM désiré ?",
                      choices=intitules,
                      ),
    ]

    # On demande à l'utilisateur de faire son choix via inquirer
    choix = inquirer.prompt(choix)

    return choix


def get_qcm(qcms, qcm_int):

    for qcm in qcms:
        if qcm['name'] == qcm_int:
            r = requests.get('http://' + SERVER + ':' + PORT + '/qcm/' + qcm['id'])
            return json.loads(r.content)


def afficher_demarage_qcm(qcm):

    os.system('clear')
    print('\n\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n\n')
    print('\t\t\t ######  ########    ###    ########  ########')
    print('\t\t\t##    ##    ##      ## ##   ##     ##    ##')
    print('\t\t\t##          ##     ##   ##  ##     ##    ##')
    print('\t\t\t ######     ##    ##     ## ########     ##')
    print('\t\t\t      ##    ##    ######### ##   ##      ##')
    print('\t\t\t##    ##    ##    ##     ## ##    ##     ##')
    print('\t\t\t ######     ##    ##     ## ##     ##    ##')
    print('\nModule : ' + qcm['module'])
    print('Début du QCM : ' + qcm['name'])
    print('\nBonne chance ! :)\n')
    print('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n\n')


def sendResponses(id_qcm, reponses, identifiant):


    corrector_server = 'http://' + CORRECTOR_SERVER + ':' + CORRECTOR_PORT + '/reponses'

    # On extrait les réponses
    rformated = []
    for r in reponses:
        rformated.append(next(iter(r.values())))

    # On construit la requête
    payload = {
        'qcm_id': id_qcm,
        'identifiant': identifiant,
        'reponses': rformated
    }


    # On envoie les réponses
    r = requests.post(
        corrector_server,
        data=json.dumps(payload))

    if r.status_code != 200:
        logger.error(colored('Erreur lors de la correction : ' + r.content, color='red'))
        return

    return json.loads(r.content)['note']


#######################################################################################################################
# Début du programme
#######################################################################################################################


# Attente d'une saisie de touche...
input()

# Affichage de la bannière
print(colored(banner, color='blue'))
print('\n\n')


user = login()
while not user:
    user = login()


while 1:

    # On récupère les modules de l'utilisateur
    modules = get_modules(user)

    #  On demande à l'utilisateur de choisir un module
    choixModule = afficher_modules(modules)

    # Si l'utilisateur souhaite retourner au menu principal
    if choixModule['res'] == 'Quitter':
        #  On quitte
        exit(0)

    # On récuères les QCMs relatifs au module choisi
    qcms = get_qcms(modules, choixModule['res'])

    # Si il n'y a pas de QCM on affiche un message d'erreur
    if not qcms:
        logger.error(colored('Aucun QCMs pour le module...', color='yellow'))

    # Sinon on continu
    else:

        # On demande à l'utilisateur de choisir un QCM
        choixQCM = afficher_qcms(qcms)

        if choixQCM['res'] != 'Retour':

            # On récupère les questions
            qcm = get_qcm(qcms, choixQCM['res'])

            if 'question' not in qcm:
                logger.error(colored('Erreur fatale, absence de questions pour le QCM choisi...', color='red'))

            else:

                # On affiche à l'utilisateurs et les informations relatives au QCM et a son lancement
                afficher_demarage_qcm(qcm)

                reponsesEtudiant=[]
                # Pour chaque question retournées par le serveur
                for q in qcm['question']:


                    # Pour chaque réponses retournées par le serveur
                    reponses = []
                    for reponse in q['reponse']:
                        reponses.append(reponse['intitule'])

                    # On contruit notre question inquirer
                    question =[
                        inquirer.Checkbox(
                            q['id'],
                            message=q['intitule'],
                            choices=reponses,
                        )
                    ]

                    # On fait passer le test à l'utilisateur et on stock ces réponses
                    reponsesEtudiant.append(inquirer.prompt(question))

                # On envoie les réponses au serveur
                score = sendResponses(qcm['id'], reponsesEtudiant, 'rortega')

                print('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
                print(colored('Votre score final est : ' + str(score), color='green'))
                print('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n\n')