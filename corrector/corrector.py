# coding: utf-8

import requests
from flask import Flask, jsonify, request, abort, json
from flask_restful import Resource, Api, reqparse
import os




# Récupération du chemin d'execution
SCRIPT_DIR = os.path.dirname(__file__)



# Définition du serveur cible
if 'DOCKER_QCM_SERVER' in os.environ:
    QCM_SERVER_ADDRESS = 'http://' + os.environ['DOCKER_QCM_SERVER'] + ':5000'
else:
    QCM_SERVER_ADDRESS = 'http://localhost:8080'

class Reponses(Resource):

    def extraire_bonnes_reponses(self, question):

        """
        Retourne la liste des réponses valides pour une question
        :param question:
        :return:
        """

        bonnes_reponses=[]
        for reponse in question['reponse']:
            if reponse['valide'] == 'True':
                bonnes_reponses.append(reponse['intitule'])

        return bonnes_reponses


    def enregistrer_reponses(self, qcm_id, reponses, identifiant):
        """
        Formatage en XML des réponses et enregistrement
        :param qcm_id:
        :param reponses:
        :param identifiant:
        """

        # Formatage

        nom_fichier = 'storage/' + str(qcm_id) + '_' + identifiant + '.xml'
        nom_fichier = os.path.join(SCRIPT_DIR, nom_fichier)

        xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n'
        xml += '<evaluation>\n'
        xml += '\t<qcm_id>' + qcm_id + '</qcm_id>\n'
        xml += '\t<identifiant>' + identifiant + '</identifiant>\n'
        xml += '\t<questions>\n'

        i = 1
        for reponse in reponses:
            xml += '\t\t<question id=\'' + str(i) + '\'>\n'

            j=1
            for subrep in reponse:
                xml += '\t\t\t<reponse id=\''+str(j)+'\'>' + subrep + '</reponse>\n'
                j += j
            xml += '\t\t</question>\n'
            i += 1

        xml += '\t</questions>\n'
        xml += '</evaluation>'

        # Enregistrement
        with open(nom_fichier, 'w+') as fichier:
            fichier.write(xml)



    def post(self):

        """
        Traite les réponses d'un étudiant et retourne la note
        :return: json
        """

        # Récupération des données transmises
        json_data = request.get_json(force=True)

        # Vérification des champs requis
        if 'qcm_id' not in json_data \
                or 'reponses' not in json_data \
                or 'identifiant' not in json_data:

            abort(403, "Les informations attendues n'ont pas été fournies.")

        # Extraction des données
        qcm_id = str(json_data['qcm_id'])
        reponses_etudiant = json_data['reponses']
        identifiant_etudiant= json_data['identifiant']

        #  On enregistre les réponses de l'étudiant
        self.enregistrer_reponses(qcm_id=qcm_id, identifiant=identifiant_etudiant, reponses=reponses_etudiant)

        # Récupération du QCM sur le serveur de QCM
        r = requests.get(QCM_SERVER_ADDRESS + '/qcm/' + qcm_id)


        if r.status_code == 404:
            abort(403, "Le QCM n'existe pas.")

        if r.status_code != 200:
            abort(503, "Une erreur est survenue.")


        # On parse le QCM
        qcm = json.loads(r.content)

        # On conscidère qu'une question vaut un point
        noteMax = len(qcm['question'])
        note = 0

        # On calcul la note
        i = 0
        for question in qcm['question']:
            reponsesValides = self.extraire_bonnes_reponses(question)

            if reponsesValides == reponses_etudiant[i]:
                note += 1

            i += 1

        # On retourne la note/noteMax

        return jsonify({
            'note': str(note) + '/' + str(noteMax)
        })






app = Flask(__name__)
api = Api(app)
api.add_resource(Reponses, '/reponses')

if __name__ == '__main__':
    app.run(debug=True,
            host='0.0.0.0')
