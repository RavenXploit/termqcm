# coding: utf-8

import requests
import xmltodict
from flask import Flask, jsonify, request, json
from flask_restful import Resource, Api, abort
import os



script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
QCMS_FILE = os.path.join(script_dir, "qcms.xml")
MODULES_FILE = os.path.join(script_dir, "modules.xml")

# Définition des serveurs cibles
if 'DOCKER_AUTH_SERVER' in os.environ:
    AUTH_SERVER = 'auth'
    AUTH_PORT = '5000'
else:
    AUTH_SERVER = 'localhost'
    AUTH_PORT = '8081'

class QCMs(Resource):


    def __init__(self):

        with open(QCMS_FILE) as fd:
            doc = xmltodict.parse(fd.read())

        self.qcms = doc['qcms']['qcm']

    def get(self, code_module):

        qcm_allowed_to_user = []

        qcms = self.qcms

        if isinstance(qcms, dict):
            if qcms['module'] == code_module:
                qcm_allowed_to_user.append({'id': qcms['id'], 'name': qcms['name']})

        else:
            for qcm in qcms:
                if qcm['module'] == code_module:
                    qcm_allowed_to_user.append({'id': qcm['id'], 'name': qcm['name']})

        return jsonify(qcm_allowed_to_user)

    def get_IDs(self):

        ids = []
        qcms = self.qcms
        if isinstance(qcms, dict):
            ids.append(qcms['id'])

        else:
            for qcm in qcms:
                ids.append(qcm['id'])

        return ids





def abort_if_qcm_doesnt_exist(qcm_id):
    q_inst = QCMs()
    ids = q_inst.get_IDs()

    if qcm_id not in ids:
        abort(404, message="QCM {} doesn't exist".format(qcm_id))

class QCM(Resource):

    def get(self, qcm_id):
        abort_if_qcm_doesnt_exist(qcm_id)

        qmanager = QCMs()
        qcms = qmanager.qcms

        if isinstance(qcms, dict):
            if qcms['id'] == qcm_id:
                encoded_qcm = jsonify(qcms)
                qcm_module = qcms['module']
        else:
            for qcm in qcms:
                if qcm['id'] == qcm_id:
                    encoded_qcm = jsonify(qcm)
                    qcm_module = qcm['module']

        return encoded_qcm

class Modules(Resource):

    def __init__(self):
        with open(MODULES_FILE) as fd:
            doc = xmltodict.parse(fd.read())

        self.modules = doc['modules']['module']

    def post(self):

        credentials = request.get_json(force=True)
        if 'token' not in credentials or not credentials['token']\
                or 'formation' not in credentials or not credentials['formation']:

                abort(403, message='Authentification required.')


        target = 'http://' + AUTH_SERVER + ':' + AUTH_PORT + '/formation/access'
        r = requests.post(url=target, data=json.dumps(credentials))

        if r.status_code == 200:
            modulesOfUser = []

            for module in self.modules:
                if module['formation'] == credentials['formation']:
                    modulesOfUser.append(module)

            return modulesOfUser

        else:
            abort(403, message='Authentification required.')


# Lancement de flask
app = Flask(__name__)
api = Api(app)



# Definition des routes
api.add_resource(Modules, '/modules')
api.add_resource(QCMs, '/qcms/<code_module>')
api.add_resource(QCM, '/qcm/<qcm_id>')


if __name__ == '__main__':
    app.run(debug=True,
            host='0.0.0.0')
