# coding: utf-8 

import os

import xmltodict
from flask import Flask, request, jsonify, json
import uuid



# Initialisation de flask
app = Flask(__name__)


# Récupération du chemin d'execution
script_dir = os.path.dirname(__file__)

# Définition de la BDD d'utilisateurs
USERS_DB = os.path.join(script_dir, "users.xml")

# Tableau permettant de stocker les utilisateurs authentifiés
allowed_users = []

@app.route('/login', methods=['POST'])
def login():
    """
    Méthode permetant de vérifier les identifiants d'un utilisateur
    :return: json  
    """
    
    # Récupération des paramètres POST au format JSON
    params = request.get_json(force=True)

    # On vérifie la présence des paramètres requis
    if 'login' not in params or 'password' not in params:
        return jsonify({'error': 'Missing parameter'}), 503

    # On charge la liste des utilisateurs
    with open(USERS_DB) as fd:
        doc = xmltodict.parse(fd.read())

    
    # On cherche une correspondance et on récupère la formation d'appartenance
    found = 0
    formation = ''
    for user in doc['users']['user']:
        if user['login'] == params['login'] and user['password'] == params['password']:
            found = 1
            formation = user['formation']
            break

    # Si on trouvé un utilisateur avec les credentials donnés
    if found:
        
        # On génère un token qui sera utilisé pour l'authentification par la suite
        token = uuid.uuid4()

        # On prépare la réponse au format json
        data = {
            'token': str(token),
            'formation': formation
        }

        # On ajoute l'étudiant dans la liste des utilisateurs authentifiés
        allowed_users.append(data)
        
        # On retourne les infos
        return app.response_class(
            response=json.dumps(data),
            status=200,
            mimetype='application/json'
        )

    # Si l'on pas trouvé l'utilisateur on retourne une 403
    else:
        return app.response_class(
            response=json.dumps({'status': 'Fail'}),
            status=403,
            mimetype='application/json'
        )


@app.route('/formation/access', methods=['POST'])
def is_valid_auth():
    """
    Méthode permetant de valider si un utilisateur est connecté ou non
    :return: json
    """
    params = request.get_json(force=True)

    if params in allowed_users:

        return jsonify({
            'status': 'Authorized'
        }), 200

    return jsonify({
        'status': 'Unauthorized'
    }), 403

# Lancement de flask
if __name__ == '__main__':
    app.run(debug=True,
            host='0.0.0.0')
